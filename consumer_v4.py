from kafka import KafkaConsumer
import psycopg2
import json
import pandas as pd
from date_convert import simple_date_to_timestamp


def add_event( id_station, id_train, id_terminal, mission_name, date, date2, delay):
    sqlU = "UPDATE event SET id_terminal = %s, mission_name=%s, date=%s, date2=%s, delay=%s, id_nextstation = %s, date_nexttrain = %s where id_station = %s AND id_train = %s;"
    sqlI = "INSERT INTO event(id_station, id_train, id_terminal, mission_name, date, date2, delay, id_nextstation, date_nexttrain) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s);"

    muser = "postgres"
    mpass = "1234"
    mhost = "54.154.137.68"
    mport = "5432"
    mdb = "traindb"
    conn = psycopg2.connect(database=mdb, user=muser, password=mpass, host=mhost, port=mport)

    # create a new cursor
    cur = conn.cursor()
	
    # select data for the next train
    cur.execute("SELECT id_station, date FROM event WHERE id_train = '{0}' AND date2 = (SELECT MIN(date2) FROM event WHERE id_train = '{0}' AND date2 > '{1}') LIMIT 1;".format(id_train, date2))
    next_train = cur.fetchone()
	
    try:
        id_nextstation = next_train[0];
    except TypeError:
        id_nextstation = None;
	
    try:
        date_nexttrain = next_train[1];
    except TypeError:
        date_nexttrain = None;

    # update
    cur.execute(sqlU, (id_terminal, mission_name, date, date2, delay, id_nextstation, date_nexttrain, id_station, id_train))

    # do insert if nothing to update
    if cur.rowcount < 1:
        cur.execute(sqlI, (id_station, id_train, id_terminal, mission_name, date, date2, delay, id_nextstation, date_nexttrain))

    # commit the changes to the database
    conn.commit()

    conn.close()

consumer = KafkaConsumer("stations", bootstrap_servers="sandbox-hdp.hortonworks.com:6667")

i = 1
max_files = 5
for message in consumer:
    val = json.loads(message.value.decode())
    print(val)
    for station in val:
        l = len(val[station])
        for j in range(1, l-1):
            train = val[station][j]
            delay = (simple_date_to_timestamp(train["date"]) - simple_date_to_timestamp(val[station][j-1]["date"])) / 60
            add_event(train["Station"], train["TrainNumber"], train["Terminal"],
                      train["TrainMission"], train["date"], 
                      simple_date_to_timestamp(train["date"]), delay)

    i += 1
    if i == max_files:
        i = 1
