import time
import requests
import pandas as pd
import json
import xmltodict
from kafka import KafkaProducer

from timer import Timer
import global_variable as glo
# Load mission and station data
station = pd.read_csv("stations2.csv", sep=";")
mission = pd.read_csv("missions.csv", sep=";")

# Set API parameters
user = "tnhtn963"
password = "t9h9iV2T"

# Set Kafka producer
producer = KafkaProducer(bootstrap_servers=glo.host)

nb_stations = station.shape[0]
timer = Timer()
max_req = 20  # Maximum number of requests per minute
i = 0
while True:
    timer.start()
    data_gare = {}
    for i in range(i, i+max_req):
        url = "https://api.transilien.com/gare/{0}/depart/".format(station.loc[i, "Code_UIC"])
        response = requests.get(url, auth=(user, password))

        # Read XML file
        gare = xmltodict.parse(response.content)
        j = 1
        if "train" in gare["passages"].keys():
            # 1st producer -----------------------------------------------------------------------------------------
            data_gare[int(station.loc[i, "Code_UIC"])] = []
            for train in gare["passages"]["train"]:
                #print(type(train["date"]))
                if type(train) == list or type(train) == str:
                    continue
                if type(train["date"]) == list:
                    continue
                print("resuming ...")
                date = train["date"]["#text"]
                num = train["num"]
                miss = train["miss"]
                term = train["term"]

                r = {"date": date,
                     "Number": j,
                     "Station": int(station.loc[i, "Code_UIC"]),
                     "Train":"RERC",
                     "TrainNumber": num,
                     "TrainMission": miss,
                     "Terminal": term}

                if train["miss"] in mission.values:
                    producer.send("api-topic", json.dumps(r).encode())
                    # print(json.dumps(r))
                    data_gare[int(station.loc[i, "Code_UIC"])].append(r)
                j += 1

        #print(i)
        if i == nb_stations-1:
            i = 0
            break
        else:
            i += 1

    # 2nd producer -----------------------------------------------------------------------------------------
    producer.send("stations", json.dumps(data_gare).encode())
    #print(data_gare)
    timer.stop()
    #print(60 - timer.interval)
    # Wait 1 minute to respect API rate
    time.sleep(60 - timer.interval)
