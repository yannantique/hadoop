# -*- coding: utf-8 -*-
"""
Created on Sat Mar  9 22:48:07 2019

@author: jean
spark requests

command = spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0 

--packages com.hortonworks:shc:1.0.0-1.6-s_2.10 --repositories http://repo.hortonworks.com/content/groups/public/ --files /opt/cloudera/parcels/CDH/lib/hbase/conf/hbase-site.xml
"""

#from pyspark import SparkConf
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession, HiveContext
#from pyspark.sql import SQLContext
import pyspark.sql.functions as F
import pyspark.sql.types as T
# from pyspark.sql.types import  T.IntegerType
from pyspark.sql.window import Window

import datetime
import time
import global_variable as glo

import os

os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0'

print("begin")
if __name__ == '__main__':
    
    
    spark_schema =  T.StructType([
                    T.StructField("date", T.StringType(), False),
                        T.StructField("Number", T.IntegerType(), False),
                            T.StructField("Station", T.IntegerType(), False),
                            T.StructField("Train", T.StringType(), False),
                                T.StructField("TrainNumber", T.IntegerType(), False), 
                                    T.StructField("TrainMission", T.StringType(), False),
                                        T.StructField("Terminal", T.IntegerType(), False)])
    print("lunching script")
    json_options = {"timestampFormat": "dd-mm-yyyy HH:mm"}
    spark = SparkSession.builder.master("yarn") \
    .appName("PythonSparkStreamingKafka_RM_01").enableHiveSupport() \
    .getOrCreate()
    
    sc = spark.sparkContext
    sqlContext = SQLContext(sc)
    hive_context = HiveContext(sc)
    sparkCores = sc.defaultParallelism
    sc.setLogLevel("ERROR")
    

    df = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", glo.host) \
    .option("subscribe", glo.kafka_topic) \
    .load()
    
    df.printSchema()
    #.schema("date TIMESTAMP, Number INT, Station INT, Train STRING, TrainNumber INT, TrainMission STRING, Terminal INT") \
    
    # TODO : convert string to timestamp
    
    df2 = df.select(F.from_json(F.col("value").cast("string"),spark_schema, json_options ).alias("tmp_data"))
    df2.printSchema()
    #df.show()
    
    #df2 = df2.select("id", "date", unix_timestamp("date", "yyyy/MM/dd HH:mm:ss").cast(TimestampType).as("timestamp"), current_timestamp(), current_date())
    #df2 = df2.select(F.to_timestamp(df.withColumn("date"), 'yyyy-MM-dd HH:mm:ss').alias('dt')).collect()
    
    #now = datetime.datetime.now()
    #now += datetime.timedelta(hours=4)
    #now_long = round(now.timestamp())
    #now_str = now.strftime('%d-%m-%Y %H:%M:%S')
    #now_timestamp = time.mktime(now.timetuple())
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%Y %H:%M:%S')
    
    df3 = df2.select("tmp_data.*")
    """
    query = df3\
    .writeStream\
    .format("console")\
    .option('truncate', False)\
    .start()
    """
    
    df3.printSchema()
    #♥df4 = df3.withColumn('date',F.unix_timestamp(F.lit(df3.date),'dd-MM-yyyy HH:mm:ss').cast("timestamp"))
    #df4 = df3.withColumn("date", df3["test"].cast(T. )
    # https://stackoverflow.com/questions/52758426/spark-sql-dataframe-how-to-read-different-format-date-format
    #df4.printSchema()
    
    
    #df4 = df3.withColumn('date2', F.unix_timestamp(df3.date, 'dd-MM-yyyy HH:mm:ss').cast("double").cast("timestamp"))
    #df4 = df3.select(df3, F.from_unixtime(F.unix_timestamp('date', 'MM/dd/yyy HH:mm')).alias("date2"))
    df4 = df3.withColumn('date2', F.unix_timestamp(df3.date, 'dd/MM/yyy HH:mm'))
    #df4 = df3.withColumn("date2",df3['date'].cast(T.DateType()))
    df4.printSchema()
    
    
     
    now2 = F.current_timestamp().cast("long")
    now2 += 3600
    #now2 = now2 - oneHour
    #df5 = df4.where(df4.date2<now2)
    
    #df5 = df4.withColumn('waiting',df4.)
    """
    w = Window.partitionBy().orderBy("date2")
    df5 = df4.withColumn("delay", F.lag(df4.date2.cast("long")).over(w))
    df5.printSchema()
    """
    #df5 = df4.toDF()
    
    query = df4\
    .writeStream\
    .format("console")\
    .option('truncate', False)\
    .start()
    
    spark.sql('show databases').show()
    spark.sql('show tables').show()
    
    
    #spark.sql('create database trains')

    #spark.sql('use trains')
    #df4.write.format('csv').save("~test.csv", header=True)
    """
    spark.sql('create table status \
    (statusId int,train string,state string,when date) \
    row format delimited fields terminated by ","\
    stored as textfile') 
    """
    
    #df5.write.csv('~/test.csv')
    #df3.toPandas().to_csv('trainsdata.csv')

    query = df4\
    .writeStream\
    .format("csv")\
    .option("path","/root/enormeFichier")\
    .option("checkpointLocation", "/root/inutile") \
    .outputMode("append") \
    .start()
    
    db = hive_context.table("test")
    db.show()
    


    
    
    #csv_data  = df4.map(lambda p: p.split(","))
    #df6 = df4.createDateFrame()
#spark.sql('show databases').show()

#spark.sql('show tables').show()
    """
    spark = SparkSession.builder.enableHiveSupport().getOrCreate()

    spark.sql('create database trains')
    
    spark.sql('use trains')
    
    
    
    spark.sql('create table status \
    (statusId int,train string,state string,when date) \
    row format delimited fields terminated by ","\
    stored as textfile') 
    
    """
    """
    query = spark\
    .withWatermark("event_time", "10 seconds")\
    .groupBy(
    F.window(F.col('event_time'), "10 seconds", "5 seconds")
    )\
    .count().writeStream\
    .outputMode("update")\
    .format("console")\
    .option('truncate',False)\
    .start()
    """
    input("kk")