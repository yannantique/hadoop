from datetime import datetime
from calendar import timegm


def simple_date_to_timestamp(dt):

    """
        Convert DateTime (YYYY-mm-dd HH:MM) to timestamp (in ms).
        :param dt: DateTime (YYYY-mm-dd HH:MM) to be converted
        :type dt: DateTime
    """

    try:

       dt = datetime.strptime(dt, "%d/%m/%Y %H:%M")

    except ValueError:

       raise ValueError("Incorrect data format, should be YYYY/mm/dd HH:MM")

    return timegm(dt.utctimetuple())
