import time


class Timer(object):

    """
        Timer to calculate time elapsed for executing a script.
    """

    def start(self):

        """
            Start the timer.
        """

        if hasattr(self, 'interval'):
            del self.interval
        self.start_time = time.time()

    def stop(self):

        """
            Stop the timer. Timer value (in seconds) can be extracted from "interval" variable.
        """

        if hasattr(self, 'start_time'):
            self.interval = round(time.time() - self.start_time, 3)
            del self.start_time  # Force timer reinit
